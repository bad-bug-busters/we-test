#ifndef GETWIND_INCLUDED
#define GETWIND_INCLUDED

float3 _WindVector;
float _WindTurbolence;
float _WindMaskScale;
float2 _WindMaskOffset;

void GetWind_float(out float3 windVector, out float windTurbolence, out float windMaskScale, out float2 windMaskOffset)
{
    windVector = _WindVector;
    windTurbolence = _WindTurbolence;
    windMaskScale = _WindMaskScale;
    windMaskOffset = _WindMaskOffset;
}

#endif