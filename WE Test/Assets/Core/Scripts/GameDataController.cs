using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

[Serializable]
public struct SimTime
{
    [Range(0, 1)] public float day;
    [Range(0, 1)] public float year;
}

public static class GameData
{
    public static bool firstTimeSetupDone;

    public static HDRenderPipelineAsset renderPiplineAsset;
    public static VolumeProfile volumeProfile;
    public static Camera camera;
    public static HDAdditionalCameraData cameraData;

    public static SimTime simTime;
    public static float sunPos;
}

[ExecuteAlways]
public class GameDataController : MonoBehaviour
{
    public VolumeProfile volumeProfile;

    void OnValidate()
    {
        Setup();
    }

    void Awake()
    {
        Setup();

        GameData.camera = Camera.main;
        GameData.cameraData = GameData.camera.GetComponent<HDAdditionalCameraData>();
    }

    void Setup()
    {
        if (!GameData.firstTimeSetupDone)
        {
            GameData.renderPiplineAsset = (HDRenderPipelineAsset)GraphicsSettings.defaultRenderPipeline;
            GameData.volumeProfile = volumeProfile;

            GameData.firstTimeSetupDone = true;
        }
    }
}
