using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor.ShaderGraph.Internal;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using Unity.Mathematics;

public enum QualityThree
{
    High,
    Medium,
    Low,
}

public enum QualityThreeOff
{
    High,
    Medium,
    Low,
    Off,
}

public enum QualityFour
{
    High,
    Medium,
    Low,
    Abysmal,
}

public enum QualityFourOff
{
    High,
    Medium,
    Low,
    Abysmal,
    Off,
}

public enum QualityFive
{
    Ultra,
    High,
    Medium,
    Low,
    Abysmal,
}

public enum QualityFiveOff
{
    Ultra,
    High,
    Medium,
    Low,
    Abysmal,
    Off,
}

public enum QualityRenderScale
{
    Native,
    Ultra,
    High,
    Medium,
    Low,
    Abysmal,
}


[Serializable]
public struct SettingsGraphics
{
    public QualityRenderScale renderScale;
    public QualityFour textures;
    public bool anisotropicFiltering;
    public QualityFive levelOfDetail;
    public QualityFiveOff shadows;
    public bool contactShadows;
    public bool microShadows;
    public QualityFiveOff ssao;
    public bool bloom;
    public bool motionBlur;
}

[Serializable]
public struct SettingsAudio
{
    public float volume;
}

[Serializable]
public struct SettingsVR
{
    public bool enableVR;
    public bool seated;
    public bool useHandControllers;
    public bool gripToggle;
    public float bodyHeight;
}

[Serializable]
public struct SettingsData
{
    public SettingsGraphics graphics;
    public SettingsAudio audio;
    public SettingsVR vr;
}

public static class GameSettings 
{
    public static bool firstTimeSetupDone;

    static string path = Application.persistentDataPath + "/Settings.json";

    public static SettingsData data = new SettingsData
    {
        graphics = new SettingsGraphics
        {
            renderScale = QualityRenderScale.High,
            textures = QualityFour.High,
            anisotropicFiltering = true,
            levelOfDetail = QualityFive.High,
            shadows = QualityFiveOff.High,
            contactShadows = true,
            microShadows = true,
            ssao = QualityFiveOff.High,
            bloom = true,
            motionBlur = true,
        },

        audio = new SettingsAudio
        {
            volume = 1,
        },

        vr = new SettingsVR
        {
            enableVR = false,
            seated = false,
            useHandControllers = true,
            gripToggle = false,
            bodyHeight = 1.75f,
        }
    };

    //public static SettingsVR VR = new SettingsVR { bodyHeight = Convert.ToSByte(175) };


    //     Callback invoked when starting up the runtime. Called before the first scene
    //     is loaded.
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    public static void SubsystemRegistration()
    {
        Debug.Log("SubsystemRegistration");
    }

    //     Callback invoked when all assemblies are loaded and preloaded assets are initialized.
    //     At this time the objects of the first scene have not been loaded yet.
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
    public static void AfterAssembliesLoaded()
    {
        Debug.Log("AfterAssembliesLoaded");
    }

    //     Callback invoked before the splash screen is shown. At this time the objects
    //     of the first scene have not been loaded yet.
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void BeforeSplashScreen()
    {
        Debug.Log("BeforeSplashScreen");
    }

    //     Callback invoked when the first scene's objects are loaded into memory but before
    //     Awake has been called.
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void BeforeSceneLoad()
    {
        Debug.Log("BeforeSceneLoad");
    }

    //     Callback invoked when the first scene's objects are loaded into memory and after
    //     Awake has been called.
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    public static void AfterSceneLoad()
    {
        Debug.Log("AfterSceneLoad");
    }

    public static class Graphics
    {
        static ShadowResolution[] shadowResolutions = new ShadowResolution[]
        {
            ShadowResolution.VeryHigh,
            ShadowResolution.High,
            ShadowResolution.Medium,
            ShadowResolution.Low,
        };
        static float[] lodBiases = new float[]
        {
            2f,
            1f,
            0.5f,
            0.25f,
            0.125f,
        };
        //static MotionBlurQuality[] motionBlurQualities = new MotionBlurQuality[]
        //{
        //    MotionBlurQuality.High,
        //    MotionBlurQuality.Medium,
        //    MotionBlurQuality.Low,
        //    MotionBlurQuality.Low,
        //};

        public static void RenderScale()
        {
            //renderPiplineAsset.renderScale = renderScales[(int)data.graphics.renderScale];
        }

        public static void Textures()
        {
            QualitySettings.globalTextureMipmapLimit = (int)data.graphics.textures;
        }

        public static void AF()
        {
            QualitySettings.anisotropicFiltering = data.graphics.anisotropicFiltering ? AnisotropicFiltering.ForceEnable : AnisotropicFiltering.Disable;
        }

        public static void LOD()
        {
            GameData.cameraData.renderingPathCustomFrameSettings.lodBias = lodBiases[(int)data.graphics.levelOfDetail];
            //QualitySettings.lodBias = lodBiases[(int)data.graphics.levelOfDetail];
        }

        public static void Shadows()
        {

        }

        public static void ContactShadows()
        {
            if (GameData.volumeProfile.TryGet(out ContactShadows contactShadows))
            {
                contactShadows.active = data.graphics.contactShadows;
            }
        }

        public static void MicroShadows()
        {
            if (GameData.volumeProfile.TryGet(out MicroShadowing microShadows))
            {
                microShadows.active = data.graphics.microShadows;
            }
        }

        public static void Bloom()
        {
            if (GameData.volumeProfile.TryGet(out Bloom bloom))
            {
                bloom.active = data.graphics.bloom;
            }
        }

        public static void MotionBlur()
        {
            if (GameData.volumeProfile.TryGet(out MotionBlur motionBlur))
            {
                motionBlur.active = data.graphics.motionBlur;
            }
        }
    }

    public static class Audio
    {
        public static void Volume()
        {
            AudioListener.volume = data.audio.volume;
        }
    }

    public static class VR
    {

    }

    public static void LoadSettings()
    {
        if (File.Exists(path))
        {
            data = JsonUtility.FromJson<SettingsData>(File.ReadAllText(path));
        }
        else
        {
            SaveSettings();
        }
    }

    public static void SaveSettings()
    {
        File.WriteAllText(path, JsonUtility.ToJson(data, true));
    }
}

[ExecuteAlways]
public class GameSettingsController : MonoBehaviour
{
    float[] renderScales = new float[]
    {
        100f,
        75f,
        200f / 3,
        50f,
        100f / 3,
        25f,
    };

    void Start()
    {
        if (!GameSettings.firstTimeSetupDone)
        {
            GameSettings.LoadSettings();

            //GameSettings.Graphics.RenderScale();
            DynamicResolutionHandler.SetDynamicResScaler(SetDynamicResolutionScale, DynamicResScalePolicyType.ReturnsPercentage);
            GameSettings.Graphics.Textures();
            GameSettings.Graphics.AF();
            GameSettings.Graphics.LOD();
            GameSettings.Graphics.Shadows();
            GameSettings.Graphics.ContactShadows();
            //SSAO
            GameSettings.Graphics.Bloom();
            GameSettings.Graphics.MotionBlur();

            GameSettings.Audio.Volume();

            GameSettings.firstTimeSetupDone = true;
            Debug.Log("Loaded and applied game settings.");
        }
    }

    public float SetDynamicResolutionScale()
    {
        return renderScales[(int)GameSettings.data.graphics.renderScale];
    }
}
