﻿using Unity.Entities;
using Unity.Physics;
using Unity.Mathematics;

public static class CastInput
{
    public static RaycastInput Ray(float3 start, float3 end, CollisionFilter filter)
    {
        return new RaycastInput
        {
            Start = start,
            End = end,
            Filter = filter
        };
    }

    public static ColliderCastInput Sphere(float3 start, float3 end, CollisionFilter filter, SphereGeometry geometry)
    {
        BlobAssetReference<Collider> collider = SphereCollider.Create(geometry, filter);
        return Input(start, end, filter, collider, quaternion.identity);
    }

    public static ColliderCastInput Box(float3 start, float3 end, CollisionFilter filter, BoxGeometry geometry, quaternion orientation)
    {
        BlobAssetReference<Collider> collider = BoxCollider.Create(geometry, filter);
        return Input(start, end, filter, collider, orientation);
    }

    public static ColliderCastInput Capsule(float3 start, float3 end, CollisionFilter filter, CapsuleGeometry geometry, quaternion orientation)
    {
        BlobAssetReference<Collider> collider = CapsuleCollider.Create(geometry, filter);
        return Input(start, end, filter, collider, orientation);
    }

    public static ColliderCastInput Cylinder(float3 start, float3 end, CollisionFilter filter, CylinderGeometry geometry, quaternion orientation)
    {
        BlobAssetReference<Collider> collider = CylinderCollider.Create(geometry, filter);
        return Input(start, end, filter, collider, orientation);
    }

    static unsafe ColliderCastInput Input(float3 start, float3 end, CollisionFilter filter, BlobAssetReference<Collider> collider, quaternion orientation)
    {
        return new ColliderCastInput
        {
            Start = start,
            End = end,
            Collider = (Collider*)collider.GetUnsafePtr(),
            Orientation = orientation
        };
    }
}
