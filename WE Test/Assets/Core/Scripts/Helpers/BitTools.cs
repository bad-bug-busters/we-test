﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ByteBool
{
    public static bool Get(byte data, int index)
    {
        return (data & 1 << index) != 0;
    }

    public static byte Set(byte data, int index, bool value)
    {
        if (value)
            data |= (byte)(1 << index);
        else
            data |= (byte)(0 << index);
        return data;
    }
}
