using Unity.Entities;
using UnityEngine;

public struct Player : IComponentData
{

}

[DisallowMultipleComponent]
public class PlayerAuthoring : MonoBehaviour
{
    class PlayerBaker : Baker<PlayerAuthoring>
    {
        public override void Bake(PlayerAuthoring authoring)
        {
            AddComponent(GetEntity(TransformUsageFlags.Dynamic), default(Player));
        }
    }
}
