using System;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using UnityEngine;
using UnityEngine.XR;

[GhostComponent(PrefabType = GhostPrefabType.AllPredicted)]
public struct PlayerInput : IInputComponentData
{
    public float moveX;
    public float moveZ;
    public float rotY;
    public bool sprint;
    public bool jump;
    public bool jumpLast;

    public float3x3 devicePos;
    public float3x3 devicePosLast;
    public float4x3 deviceRot;
    public float4x3 deviceRotLast;
    public bool2 trigger;
    public bool2 triggerLast;
    public bool2 grip;
    public bool2 gripLast;
}

[DisallowMultipleComponent]
public class PlayerInputAuthoring : MonoBehaviour
{
    class CubeInputBaking : Baker<PlayerInputAuthoring>
    {
        public override void Bake(PlayerInputAuthoring authoring)
        {
            var entity = GetEntity(TransformUsageFlags.Dynamic);
            AddComponent<PlayerInput>(entity);
        }
    }
}

[UpdateInGroup(typeof(GhostInputSystemGroup))]
public partial struct PlayerInputSystem : ISystem
{
    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
        state.RequireForUpdate<PlayerSpawner>();
        state.RequireForUpdate<PlayerInput>();
        state.RequireForUpdate<NetworkId>();
    }

    public void OnUpdate(ref SystemState state)
    {
        foreach (var playerInput in SystemAPI.Query<RefRW<PlayerInput>>().WithAll<GhostOwnerIsLocal>())
        {
            playerInput.ValueRW = default;
            playerInput.ValueRW.moveX = Input.GetAxis("Horizontal");
            playerInput.ValueRW.moveZ = Input.GetAxis("Vertical");
            playerInput.ValueRW.rotY = math.clamp(Input.GetAxis("Rotate Horizontal"), -1, 1);
            playerInput.ValueRW.sprint = Input.GetButton("Sprint");
            playerInput.ValueRW.jump = Input.GetButton("Jump");

            for (int i = 0; i < 3; i++)
            {
                var device = InputDevices.GetDeviceAtXRNode(i == 0 ? XRNode.CenterEye : (i == 1 ? XRNode.LeftHand : XRNode.RightHand));
                if (device.isValid)
                {
                    if (device.TryGetFeatureValue(CommonUsages.devicePosition, out Vector3 pos))
                    {
                        playerInput.ValueRW.devicePos[i] = pos;
                    }
                    if (device.TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion rot))
                    {
                        playerInput.ValueRW.deviceRot[i] = new float4(rot.x, rot.y, rot.z, rot.w);
                    }
                }
            }

            for (int i = 0; i < 2; i++)
            {
                playerInput.ValueRW.trigger[i] = Convert.ToBoolean(Input.GetAxis(i == 0 ? "Trigger Left" : "Trigger Right"));
                playerInput.ValueRW.grip[i] = Convert.ToBoolean(Input.GetAxis(i == 0 ? "Grip Left" : "Grip Right"));
            }
        }
    }
}
