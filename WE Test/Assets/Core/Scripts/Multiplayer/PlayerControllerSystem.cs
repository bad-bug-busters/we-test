using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine.UIElements;
using UnityEngine.UIElements.Experimental;

[UpdateInGroup(typeof(BeforePhysicsSystemGroup))]
public partial struct PlayerControllerSystem : ISystem
{
    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
        var builder = new EntityQueryBuilder(Allocator.Temp)
            .WithAll<Simulate>()
            .WithAll<PlayerInput>()
            .WithAllRW<LocalTransform>();
        var query = state.GetEntityQuery(builder);
        state.RequireForUpdate(query);
    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        var moveJob = new PlayerControllerJob
        {
            deltaTime = SystemAPI.Time.DeltaTime,
        };
        state.Dependency = moveJob.ScheduleParallel(state.Dependency);
    }

    [BurstCompile]
    [WithAll(typeof(Simulate))]
    partial struct PlayerControllerJob : IJobEntity
    {
        public float deltaTime;

        public void Execute(PlayerInput playerInput, ref PhysicsVelocity vel, in PhysicsMass mass, in LocalTransform trans, in Player player)
        {
            vel.Angular = new float3(0, playerInput.rotY * 4, 0);

            if (playerInput.moveX != 0 || playerInput.moveZ != 0)
            {
                var calcE = MathNew.ToEulerAngles(playerInput.deviceRot.c1);
                var finalDir = math.rotate(math.mul(playerInput.deviceRot.c1, quaternion.RotateX(math.radians(-calcE.x))), new float3(0, 0, 1));
                finalDir.y = 0;
                var heading = quaternion.LookRotationSafe(finalDir, math.up());

                var impulse = math.rotate(math.mul(trans.Rotation, heading), math.normalizesafe(new float3(playerInput.moveX, 0, playerInput.moveZ)));
                impulse *= playerInput.sprint ? 50 : 25;
                impulse /= math.length(vel.Linear) + 1;

                vel.ApplyLinearImpulse(mass, impulse);
            }

            //handle hmd offset

            //var offset = playerInput.devicePos.c0 - playerInput.devicePosLast.c0;
            //offset.y = 0;

            //vel.Linear += math.rotate(trans.Rotation, offset) / deltaTime;
        }
    }
}
