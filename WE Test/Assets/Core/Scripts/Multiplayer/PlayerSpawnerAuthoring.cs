using Unity.Entities;
using UnityEngine;

public struct PlayerSpawner : IComponentData
{
    public Entity Player;
}

[DisallowMultipleComponent]
public class PlayerSpawnerAuthoring : MonoBehaviour
{
    public GameObject Player;

    class PlayerSpawnerBaker : Baker<PlayerSpawnerAuthoring>
    {
        public override void Bake(PlayerSpawnerAuthoring authoring)
        {
            var playerSpawner = default(PlayerSpawner);
            playerSpawner.Player = GetEntity(authoring.Player, TransformUsageFlags.Dynamic);
            AddComponent(GetEntity(TransformUsageFlags.Dynamic), playerSpawner);
        }
    }
}
