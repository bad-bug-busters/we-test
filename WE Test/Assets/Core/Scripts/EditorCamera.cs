﻿using UnityEngine;
using UnityEditor;

[ExecuteAlways]
public class EditorCamera : MonoBehaviour
{
    public bool playMode;
    Transform sceneCameraTrans;

    void Awake()
    {
        ResetCamera();
    }

#if UNITY_EDITOR
    void LateUpdate()
    {
        if (!Application.isPlaying)
        {
            if (sceneCameraTrans == null)
                sceneCameraTrans = SceneView.lastActiveSceneView.camera.transform;
            else
            {
                transform.position = sceneCameraTrans.position;
                transform.rotation = sceneCameraTrans.rotation;
            }
        }
    }

    void OnDisable()
    {
        ResetCamera();
    }
#endif

    void ResetCamera()
    {
        if (!playMode)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
    }
}