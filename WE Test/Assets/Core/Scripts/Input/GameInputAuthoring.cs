using System;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using UnityEngine;
using UnityEngine.XR;
using static Unity.Entities.SystemAPI;

public struct InputGeneral : IComponentData
{
    public float moveX;
    public float moveZ;
    public float rotY;
    public float stance;
    public bool sprint;
    public bool jump;
    public bool kick;
}

public struct InputVRHead : IComponentData
{
    public float3 pos;
    public quaternion rot;
}

public struct InputNoVRHead : IComponentData
{
    public float rotX;
    public bool freeLook;
}

public struct InputVRHands : IComponentData
{
    public float3x2 pos;
    public float4x2 rot;
    public bool2 trigger;
    public bool2 grip;
}

public struct InputNoVRHands : IComponentData
{
    public bool primaryAttack;
    public bool secondaryAttack;
    public bool block;
    public bool offhandAttack;
    public bool interact;
    public bool drop;
}

public static class GameInput
{
    public static InputDevice[] inputDevices = new InputDevice[3];
}

[DisallowMultipleComponent]
public class GameInputAuthoring : MonoBehaviour
{
    class GameInputBaking : Baker<GameInputAuthoring>
    {
        public override void Bake(GameInputAuthoring authoring)
        {
            var entity = GetEntity(TransformUsageFlags.Dynamic);
            AddComponent<InputGeneral>(entity);
        }
    }
}

[UpdateInGroup(typeof(GhostInputSystemGroup))]
public partial struct GameInputSystem : ISystem, ISystemStartStop
{
    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
        state.RequireForUpdate<InputGeneral>();
    }

    public void OnStartRunning(ref SystemState state)
    {
        var entity = GetSingletonEntity<InputGeneral>();
        if (GameSettings.data.vr.enableVR)
        {
            for (int i = 0; i < GameInput.inputDevices.Length; i++)
            {
                GameInput.inputDevices[i] = InputDevices.GetDeviceAtXRNode(i == 0 ? XRNode.CenterEye : (i == 1 ? XRNode.LeftHand : XRNode.RightHand));
                Debug.Log(GameInput.inputDevices[i].name + " " + GameInput.inputDevices[i].manufacturer + " " + GameInput.inputDevices[i].serialNumber);
            }
        }


        if (GameSettings.data.vr.enableVR)
        {
            state.EntityManager.AddComponent<InputVRHead>(entity);
        }
        else
        {
            state.EntityManager.AddComponent<InputNoVRHead>(entity);
        }

        if (GameSettings.data.vr.enableVR && GameSettings.data.vr.useHandControllers)
        {
            state.EntityManager.AddComponent<InputVRHands>(entity);
        }
        else
        {
            state.EntityManager.AddComponent<InputNoVRHands>(entity);
        }
    }

    public void OnUpdate(ref SystemState state)
    {
        var inputGeneral = GetSingletonRW<InputGeneral>();
        inputGeneral.ValueRW.moveX = Input.GetAxis("Horizontal");
        inputGeneral.ValueRW.moveZ = Input.GetAxis("Vertical");
        inputGeneral.ValueRW.rotY = math.clamp(Input.GetAxis("Rotate Horizontal"), -1, 1);
        inputGeneral.ValueRW.sprint = Input.GetButton("Sprint");
        inputGeneral.ValueRW.jump = Input.GetButton("Jump");

        RefRW<InputVRHead> inputVRHead;
        RefRW<InputNoVRHead> inputNoVRHead;
        if (TryGetSingletonRW(out inputVRHead))
        {
            if (GameInput.inputDevices[0].TryGetFeatureValue(CommonUsages.devicePosition, out Vector3 pos))
            {
                inputVRHead.ValueRW.pos = pos;
            }
            if (GameInput.inputDevices[0].TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion rot))
            {
                inputVRHead.ValueRW.rot = rot;
            }
        }
        else if (TryGetSingletonRW(out inputNoVRHead))
        {
            inputNoVRHead.ValueRW.rotX = Convert.ToSByte(math.clamp(Input.GetAxis("Rotate Vertical"), -1, 1));
            inputNoVRHead.ValueRW.freeLook = Input.GetButton("Free Look");
        }

        RefRW<InputVRHands> inputVRHands;
        RefRW<InputNoVRHands> inputNoVRHands;
        if (TryGetSingletonRW(out inputVRHands))
        {
            for (int i = 0; i < 2; i++)
            {
                inputVRHands.ValueRW.trigger[i] = Convert.ToBoolean(Input.GetAxis(i == 0 ? "Trigger Left" : "Trigger Right"));
                inputVRHands.ValueRW.grip[i] = Convert.ToBoolean(Input.GetAxis(i == 0 ? "Grip Left" : "Grip Right"));

                var j = i++;
                if (GameInput.inputDevices[j].TryGetFeatureValue(CommonUsages.devicePosition, out Vector3 pos))
                {
                    inputVRHands.ValueRW.pos[i] = pos;
                }
                if (GameInput.inputDevices[j].TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion rot))
                {
                    inputVRHands.ValueRW.rot[i] = new float4(rot.x, rot.y, rot.z, rot.w);
                }
            }
        }
        else if (TryGetSingletonRW(out inputNoVRHands))
        {
            inputNoVRHands.ValueRW.primaryAttack = Input.GetButton("Interact");
            inputNoVRHands.ValueRW.secondaryAttack = Input.GetButton("Interact");
            inputNoVRHands.ValueRW.block = Input.GetButton("Interact");
            inputNoVRHands.ValueRW.offhandAttack = Input.GetButton("Interact");
            inputNoVRHands.ValueRW.interact = Input.GetButton("Interact");
        }
    }

    [BurstCompile]
    public void OnStopRunning(ref SystemState state)
    {

    }
}
