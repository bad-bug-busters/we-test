using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(TransformSystemGroup))]
[WorldSystemFilter(WorldSystemFilterFlags.ClientSimulation)]
public partial class XRRigSystem : SystemBase
{
    Transform rig;

    protected override void OnCreate()
    {
        RequireForUpdate<GhostOwnerIsLocal>();
        RequireForUpdate<PlayerInput>();
    }

    protected override void OnStartRunning()
    {
        rig = GameObject.FindWithTag("XRRig").transform;
    }

    protected override void OnUpdate()
    {
        var localPlayer = SystemAPI.GetSingletonEntity<PlayerInput>();
        var trans = SystemAPI.GetComponent<LocalTransform>(localPlayer);

        //var offset = SystemAPI.GetComponent<PlayerInput>(localPlayer).devicePos.c0;
        //offset.y = 0;

        rig.position = trans.Position /*- math.rotate(trans.Rotation, offset)*/;
        rig.rotation = trans.Rotation;
    }
}
