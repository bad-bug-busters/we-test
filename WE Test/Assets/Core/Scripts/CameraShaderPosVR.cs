using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class CameraShaderPosVR : MonoBehaviour
{
    void Update()
    {
        Shader.SetGlobalVector("_CamPos", transform.position);
    }
}
