﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Authoring;
using Unity.Physics.Extensions;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

[System.Serializable]
public struct Friction : IComponentData
{
    public float crossSection;
    public float dragCoefficient;
    public float3 centerOfPressure;
    public float3 dragSpin;
    public bool skipAngular;
    [HideInInspector]public float drag;
}

[DisallowMultipleComponent]
public class FrictionAuthoring : MonoBehaviour
{
    public bool autoSetup = true;
    public Friction friction = new Friction
    {
        crossSection = math.PI * 0.25f,
        dragCoefficient = 0.43f,
    };

    class FrictionBaker : Baker<FrictionAuthoring>
    {
        public override void Bake(FrictionAuthoring authoring)
        {
            AddComponent(GetEntity(TransformUsageFlags.Dynamic), authoring.friction);
        }
    }

    void OnValidate()
    {
        if (TryGetComponent(out Rigidbody rb))
        {
            rb.drag = 0;
            rb.angularDrag = 0;
        }

        if (TryGetComponent(out PhysicsBodyAuthoring pba))
        {
            pba.LinearDamping = 0;
            pba.AngularDamping = 0;
        }

        if (autoSetup && TryGetComponent(out UnityEngine.Collider collider))
        {
            friction.crossSection = math.PI * math.pow((collider.bounds.extents.x + collider.bounds.extents.y + collider.bounds.extents.z) / 3, 2);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position + transform.rotation * friction.centerOfPressure, math.sqrt(friction.crossSection) / math.PI);

        if (TryGetComponent(out Rigidbody rb))
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position + transform.rotation * rb.centerOfMass * transform.localScale.y, math.pow(rb.mass / (4f / 3f * Mathf.PI), 1f / 3f) * 0.1f);
        }
    }
}

[UpdateInGroup(typeof(BeforePhysicsSystemGroup))]
public partial struct FrictionSystem : ISystem, ISystemStartStop
{
    [BurstCompile]
    public void OnStartRunning(ref SystemState state)
    {
        var density = 1.204f;
        foreach (var friction in SystemAPI.Query<RefRW<Friction>>())
        {
            friction.ValueRW.drag = -0.5f * density * friction.ValueRO.dragCoefficient * friction.ValueRO.crossSection;
        }
    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        var frictionJob = new FrictionJob
        {
            deltaTime = SystemAPI.Time.DeltaTime,
        };
        state.Dependency = frictionJob.ScheduleParallel(state.Dependency);
    }

    [BurstCompile]
    public void OnStopRunning(ref SystemState state)
    {
    }

    [BurstCompile]
    partial struct FrictionJob : IJobEntity
    {
        public float deltaTime;

        public void Execute(ref PhysicsVelocity vel, in PhysicsMass mass, in LocalTransform trans, in Friction friction)
        {
            //apply spin
            if (friction.dragSpin.x != 0 || friction.dragSpin.y != 0 || friction.dragSpin.z != 0)
            {
                vel.ApplyAngularImpulse(mass, friction.dragSpin * math.length(vel.Linear) * deltaTime);
            }
                
            var point = trans.Position + math.rotate(trans.Rotation, friction.centerOfPressure);
            var dragFactor = friction.drag * deltaTime;

            var linearDrag = math.normalizesafe(vel.Linear) * math.lengthsq(vel.Linear) * dragFactor;
            vel.ApplyImpulse(mass, trans.Position, trans.Rotation, linearDrag, point);

            if (!friction.skipAngular)
            {
                var angularDrag = math.normalizesafe(vel.Angular) * math.lengthsq(vel.Angular) * dragFactor;
                vel.ApplyAngularImpulse(mass, angularDrag);
            }
        }
    }
}
