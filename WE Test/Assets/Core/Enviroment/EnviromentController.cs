﻿using JetBrains.Annotations;
using System;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

[ExecuteAlways]
public class EnviromentController : MonoBehaviour
{
    public SimTime simTime;

    [Range(1, 86400)] public float secondsPerDay = 86400;
    [Range(1, 365)] public float daysPerYear = 365;

    public bool simulate;
    [Range(0, 3600)] public float updateRate = 1;

    [Header("Earth")]
    [Range(-90, 90)] public float latitude = 45;
    [Range(-180, 180)] public float longitude;
    [Range(-45, 45)] public float axialTilt = 23.5f;
    public Transform outerTerrain;

    [Header("Sun")]
    public Transform sunTransform;
    public HDAdditionalLightData sunLightData;

    [Header("Moon")]
    public Transform moonTransform;
    public HDAdditionalLightData moonLightData;
    [Range(0, 1)] public float moonPhase = 0.5f;
    public Texture2D[] moonPhaseSprites;

    //public Wind wind;
    [Header("Lighting")]
    public bool dimLights;
    [Range(-0.75f, 0f)] public float lightSwitch = -0.125f;
    [Range(-0.75f, 0f)] public float shadowSwitch = -0.0625f;
    [Range(0, 0.25f)] public float shadowBlend = 0.0625f;

    quaternion fixedRot;
    quaternion planetRot;

    HDRenderPipeline hdrp;
    //VisualEnvironment env;
    PhysicallyBasedSky pbs;
    Fog fog;

    float updateTime;

    float dayTimeMod;
    float yearTimeMod;

    float2 windVector;

    void OnValidate()
    {
        Setup();
        UpdateSky();
    }

    void Start()
    {
        Setup();
    }

    void LateUpdate()
    {
        if (simulate)
        {
            GameData.simTime.day += dayTimeMod * Time.deltaTime;
            GameData.simTime.day %= 1f;

            GameData.simTime.year += yearTimeMod * Time.deltaTime;
            GameData.simTime.year %= 1f;

            if (Time.time >= updateTime)
            {
                updateTime += updateRate;
                UpdateSky();
            }
        }
        //wind.maskOffset += windVector * Time.deltaTime;
        //Shader.SetGlobalVector("_WindMaskOffset", (Vector2)wind.maskOffset);

        //EnviromentData.time = time;
        //EnviromentData.wind = wind;
    }

    void Setup()
    {
        GameData.simTime = simTime;

        updateTime = Time.time;
        dayTimeMod = 1f / secondsPerDay;
        yearTimeMod = 1f / (secondsPerDay * daysPerYear);

        fixedRot = quaternion.EulerXYZ(math.radians(90 - latitude), math.radians(180), 0);
        planetRot = math.mul(quaternion.RotateX(math.radians(axialTilt)), quaternion.RotateY(math.radians(GameData.simTime.year * 360)));

        hdrp = (HDRenderPipeline)RenderPipelineManager.currentPipeline;

        //VisualEnvironment envTmp;
        //if (GameData.volumeProfile.TryGet(out envTmp))
        //{
        //    env = envTmp;
            //env.windOrientation.value = wind.orientation;
            //env.windSpeed.value = wind.speed;

            //var orientationRad = math.radians(wind.orientation);
            //var windDirection = new float2(math.cos(orientationRad), math.sin(orientationRad));
            //windVector = windDirection * wind.speed / 3.6f;

            //Shader.SetGlobalVector("_WindVector", new Vector3(windVector.x, 0, windVector.y));
            //Shader.SetGlobalFloat("_WindTurbolence", wind.turbolence);
            //Shader.SetGlobalTexture("_WindMaskTexture", wind.maskTexture);
            //Shader.SetGlobalFloat("_WindMaskScale", wind.maskScale);
        //}

        if (GameData.volumeProfile.TryGet(out pbs))
        {
            pbs.planetRotation.Override(math.degrees(MathNew.ToEulerAngles(math.mul(quaternion.RotateX(math.radians(-90 + latitude)), quaternion.RotateY(math.radians(longitude))))));
        }

        if (GameData.volumeProfile.TryGet(out fog))
        {
            //float fogDistance = math.sqrt(math.pow(pbs.planetaryRadius.value + fog.maximumHeight.value, 2) - math.pow(pbs.planetaryRadius.value, 2));
            float fogDistance = 3571.8346134227715667222109707422f * math.sqrt(fog.maximumHeight.value);//distance to horizon
            fog.maxFogDistance.Override(fogDistance);
            fog.mipFogFar.Override(fogDistance);
        }

        if (moonPhaseSprites.Length != 0)
        {
            moonLightData.surfaceTexture = moonPhaseSprites[(int)(moonPhase * moonPhaseSprites.Length)];
        }

        //var scale = 2 * math.PI * pbs.planetaryRadius.value / 360;
        //var latScale = scale * math.cos(math.radians(planet.latitude));
        //Debug.Log(latScale);
        //outerTerrain.localScale = new Vector3(latScale, scale, scale);
        //outerTerrain.position = new Vector3(planet.longitude * latScale, 0, planet.latitude * scale);
    }

    void UpdateSky()
    {
        var timeRot = quaternion.RotateY(math.radians((GameData.simTime.day - GameData.simTime.year) * 360));
        var dotVal = new float[2];
        for (int i = 0; i < 2; i++)
        {
            var transform = i == 0 ? sunTransform : moonTransform;
            transform.rotation = math.mul(fixedRot, math.mul(timeRot, math.mul(planetRot, quaternion.RotateY(math.radians(i == 0 ? 180 : 180 - moonPhase * 360)))));

            dotVal[i] = math.dot(transform.rotation * Vector3.forward, Vector3.down);

            var lightData = i == 0 ? sunLightData : moonLightData;
            var color = Color.white;
            if (dimLights)
            {
                var value = 1 - math.clamp(dotVal[i], lightSwitch, 0) / lightSwitch;
                color = new Color(value, value, value);
            }
            lightData.color = color;
            lightData.shadowDimmer = 1 - math.saturate((i == 0 ? shadowSwitch + shadowBlend - dotVal[0] : dotVal[0] - shadowSwitch + shadowBlend) / shadowBlend);
            lightData.EnableShadows(dotVal[0] >= shadowSwitch ? (i == 0) : (i != 0));
        }
        var sunPos = 1 - math.acos(dotVal[0]) / math.PI;
        GameData.sunPos = GameData.simTime.day < 0.5f ? -sunPos : sunPos;
        //Debug.Log("sun: " + EnviromentData.sunPos);

        pbs.spaceRotation.Override(math.degrees(MathNew.ToEulerAngles(math.mul(fixedRot, quaternion.RotateY(math.radians((GameData.simTime.year + GameData.simTime.day) * 360 + 180))))));
        if (hdrp != null)
        {
            hdrp.RequestSkyEnvironmentUpdate();
        }
    }
}
