using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

//using UnityEngine.Rendering;

public class MainMenuController : MonoBehaviour
{
    VisualElement RVE;

    //[Range(0, 100)] public float resScale = 50;
    //public float SetDynamicResolutionScale()
    //{
    //    return resScale;
    //}

    //void Start()
    //{
    //    DynamicResolutionHandler.SetDynamicResScaler(SetDynamicResolutionScale, DynamicResScalePolicyType.ReturnsPercentage);
    //}

    void OnEnable()
    {
        RVE = GetComponent<UIDocument>().rootVisualElement;

        //Graphics
        RVE.Q<EnumField>("GS-RenderScale").RegisterValueChangedCallback((evt) =>
        {
            Enum.TryParse(evt.newValue.ToString(), out QualityRenderScale value);
            GameSettings.data.graphics.renderScale = value;
            GameSettings.Graphics.RenderScale();
        });
        //AA
        RVE.Q<EnumField>("GS-Textures").RegisterValueChangedCallback((evt) =>
        {
            Enum.TryParse(evt.newValue.ToString(), out QualityFour value);
            GameSettings.data.graphics.textures = value;
            GameSettings.Graphics.Textures();
        });
        RVE.Q<Toggle>("GS-AF").RegisterValueChangedCallback((evt) =>
        {
            GameSettings.data.graphics.anisotropicFiltering = evt.newValue;
            GameSettings.Graphics.AF();
        });
        RVE.Q<EnumField>("GS-LOD").RegisterValueChangedCallback((evt) =>
        {
            Enum.TryParse(evt.newValue.ToString(), out QualityFive value);
            GameSettings.data.graphics.levelOfDetail = value;
            GameSettings.Graphics.LOD();
        });
        RVE.Q<EnumField>("GS-Shadows").RegisterValueChangedCallback((evt) =>
        {
            Enum.TryParse(evt.newValue.ToString(), out QualityFiveOff value);
            GameSettings.data.graphics.shadows = value;
            GameSettings.Graphics.Shadows();
        });
        RVE.Q<Toggle>("GS-ContactShadows").RegisterValueChangedCallback((evt) =>
        {
            GameSettings.data.graphics.contactShadows = evt.newValue;
            GameSettings.Graphics.ContactShadows();
        });
        RVE.Q<Toggle>("GS-MicroShadows").RegisterValueChangedCallback((evt) =>
        {
            GameSettings.data.graphics.microShadows = evt.newValue;
            GameSettings.Graphics.MicroShadows();
        });
        //SSAO
        RVE.Q<Toggle>("GS-Bloom").RegisterValueChangedCallback((evt) =>
        {
            GameSettings.data.graphics.bloom = evt.newValue;
            GameSettings.Graphics.Bloom();
        });
        RVE.Q<Toggle>("GS-MotionBlur").RegisterValueChangedCallback((evt) =>
        {
            GameSettings.data.graphics.motionBlur = evt.newValue;
            GameSettings.Graphics.MotionBlur();
        });

        //Audio
        RVE.Q<Slider>("AS-Volume").RegisterValueChangedCallback((evt) =>
        {
            GameSettings.data.audio.volume = evt.newValue;
            GameSettings.Audio.Volume();
        });

        RVE.Q<Button>("SaveSettings").clicked += () =>
        {
            GameSettings.SaveSettings();
        };

        RVE.Q<Button>("Quit").clicked += () =>
        {
            Debug.Log("Quit");
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        };
    }
}
